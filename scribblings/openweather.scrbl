#lang scribble/manual
@require[@for-label[openweather
                    tjson
                    racket/base]]

@title{OpenWeather}
@author[(author+email "Raymond Racine" "ray.racine@gmail.com")]

@defmodule[openweather]

@defthing[#:kind "Type" App-Id]{
String

(define-new-subtype App-Id (app-id String))

The App Identifier assigned by OpenWeatherMap when you sign up for an account.
}

@defthing[#:kind "Type" Zipcode]{
String

@racket[(define-new-subtype Zipcode (zipcode String))]

US Zipcode value for the requested weather information.
}

@defstruct*[OpenWeatherMap
  ((datetime    Exact-Nonnegative-Integer)
   (city        String)
   (clouds      Exact-Nonnegative-Integer)
   (visibility  Exact-Nonnegative-Integer)
   (humidity    Exact-Nonnegative-Integer)
   (pressure    Exact-Nonnegative-Integer)
   (temp        Exact-Nonnegative-Integer)
   (temp-min    Exact-Nonnegative-Integer)
   (temp-max    Exact-Nonnegative-Integer)
   (sunrise     Exact-Nonnegative-Integer)
   (sunset      Exact-Nonnegative-Integer))
  #:transparent]{

  Data structure for the weather values parsed from the API's returned Json.
  Temperature units are converted to Fahrenheit from API's Kelvin.
  All other values are as stated in OpenWeatherMap's API documentation.
  }

@defproc[(owm-invoke-json (aid App-Id)(zip Zipcode)) (Option JsObject)]{
Performs an HTTP call to the OpenWeatherMap free API for the given zipcode in the US.
Returns the Json response parsed into a JsObject as in OpenWeatherMap's API documentation.
}

@defproc[(owm-invoke-json (aid App-Id)(zip Zipcode)) (Option JsObject)]{
Performs an HTTP call to the OpenWeatherMap free API for the given zipcode in the US.
OpenWeatherMap's Json response is parsed into @racket[OpenWeatherMap] struct.
}