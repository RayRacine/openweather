#lang typed/racket

#| 
Invoke the OpenWeatherMap API.
Current support is US and calling by zipcode.
Datetimes are Unix timestamp.
Temperatures are in Fahrenheit.
Cloud cover is percent.
|#

(provide
 (struct-out OpenWeatherMap)
 App-Id app-id
 Zipcode zipcode
 owm-invoke
 owm-invoke-json)
 
(require
 (only-in opt/either
          right)
 (only-in uri/url
          parse-url
          QParam QParams
          Url Url-query)
 (only-in http11
          http-invoke http-successful?
          http-close-connection
          HTTPConnection-in)
 (only-in tjson
          JsObject
          read-json))

(define-new-subtype App-Id   (app-id String))
(define-new-subtype Zipcode (zipcode String))

(struct OpenWeatherMap
  ((datetime   : Exact-Nonnegative-Integer)
   (city       : String)
   (clouds     : Exact-Nonnegative-Integer)
   (visibility : Exact-Nonnegative-Integer)
   (humidity   : Exact-Nonnegative-Integer)
   (pressure   : Exact-Nonnegative-Integer)
   (temp       : Exact-Nonnegative-Integer)
   (temp-min   : Exact-Nonnegative-Integer)
   (temp-max   : Exact-Nonnegative-Integer)
   (sunrise    : Exact-Nonnegative-Integer)
   (sunset     : Exact-Nonnegative-Integer))
  #:transparent)

(: owm-base-uri Url)
(define owm-base-uri
  (right (parse-url "http://api.openweathermap.org/data/2.5/weather")))

(: invoke-url-zip-params (-> App-Id Zipcode QParams))
(define (invoke-url-zip-params appid zip)
  (define key-param (QParam "appid" appid))
  (define zip-param (QParam "zip" (string-append zip ",us")))
  (list key-param zip-param))

(: owm-invoke-url (-> App-Id Zipcode Url))
(define (owm-invoke-url appid zip)
  (struct-copy Url owm-base-uri (query (invoke-url-zip-params appid zip))))

(: owm-invoke-json (-> App-Id Zipcode (Option JsObject)))
(define (owm-invoke-json aid zip)
  (let* ((url (owm-invoke-url aid zip))
         (conn (http-invoke 'GET url '() #f)))
    (if (http-successful? conn)
        (let ((jdata (read-json (HTTPConnection-in conn))))
          (http-close-connection conn)
          (if (hash? jdata)
              jdata
              #f))
        (begin
          (http-close-connection conn)
          #f))))

(: lookup-string (-> JsObject Symbol (Option String)))
(define (lookup-string jobj key)
  (let ((v (hash-ref jobj key)))
    (if (string? v)
        v
        #f)))

(: lookup-integer (-> JsObject Symbol (Option Nonnegative-Integer)))
(define (lookup-integer jobj key)
  (let ((v (hash-ref jobj key)))
    (if (number? v)
        (let ((i (cond
                   ((exact-integer? v) v)
                   ((real? v)
                    (inexact->exact (round v)))
                   (else #f))))
          (if (exact-nonnegative-integer? i)
              i
              #f))
        #f)))
        
(: cloud-cover (-> JsObject (Option Exact-Nonnegative-Integer)))
(define (cloud-cover jobj)
  (let ((vs (hash-ref jobj 'clouds)))
    (if (hash? vs)
        (lookup-integer vs 'all)
        #f)))

(: kelvin->fahrenheit (-> Real Exact-Nonnegative-Integer))
(define (kelvin->fahrenheit k)
  (let ((f (exact-round (+ (* (- k 273.15) 1.8) 32))))
    (if (exact-nonnegative-integer? f)
        f
        (error 'KELVIN->FAHRENHEIT))))

(: main-weather (-> JsObject (Vector (Option Exact-Nonnegative-Integer)
                                     (Option Exact-Nonnegative-Integer)
                                     (Option Exact-Nonnegative-Integer)
                                     (Option Exact-Nonnegative-Integer)
                                     (Option Exact-Nonnegative-Integer))))
(define (main-weather jobj)
  (let ((vs (hash-ref jobj 'main)))
    (if (hash? vs)
        (let ((h (lookup-integer vs 'humidity))
              (p (lookup-integer vs 'pressure))
              (t (lookup-integer vs 'temp))
              (t-min (lookup-integer vs 'temp_min))
              (t-max (lookup-integer vs 'temp_max)))
          (vector h p t t-min t-max))
        (vector #f #f #f #f #f))))

(: sun-times (-> JsObject (Vector (Option Exact-Nonnegative-Integer)
                                  (Option Exact-Nonnegative-Integer))))
(define (sun-times jobj)
  (let ((vs (hash-ref jobj 'sys)))
    (if (hash? vs)
        (let ((sunrise (lookup-integer vs 'sunrise))
              (sunset  (lookup-integer vs 'sunset)))
          (vector sunrise sunset))
        (vector #f #f))))
  
(: owm-invoke (-> App-Id Zipcode (Option OpenWeatherMap)))
(define (owm-invoke aid zip)
  (let ((jdata (owm-invoke-json aid zip)))
    (if jdata
        (let ((datetime (lookup-integer jdata 'dt))
              (vis      (lookup-integer jdata 'visibility))
              (clouds   (cloud-cover jdata))
              (weather  (main-weather jdata))
              (sun      (sun-times jdata))
              (city     (lookup-string jdata 'name)))
          (let ((h (vector-ref weather 0))
                (p (vector-ref weather 1))
                (t (vector-ref weather 2))
                (t-min (vector-ref weather 3))
                (t-max (vector-ref weather 4))
                (sr (vector-ref sun 0))
                (ss (vector-ref sun 1)))
            (if (and datetime city clouds vis h p t t-min t-max sr ss)
                (OpenWeatherMap datetime city clouds vis h p
                                (kelvin->fahrenheit t)
                                (kelvin->fahrenheit t-min)
                                (kelvin->fahrenheit t-max)
                                sr ss)
                #f)))
        #f)))

;;'http://api.openweathermap.org/data/2.5/weather?zip=33497,us&appid=<appid>'

;; '#hasheq((base . "stations")
;;          (clouds . #hasheq((all . 40)))
;;          (cod . 200)
;;          (coord . #hasheq((lat . 26.4) (lon . -80.19)))
;;          (dt . 1543763700)
;;          (id . 420008724)
;;          (main
;;           .
;;           #hasheq((humidity . 75)
;;                   (pressure . 1017)
;;                   (temp . 301.16)
;;                   (temp_max . 302.05)
;;                   (temp_min . 299.85)))
;;          (name . "Boca Raton")
;;          (sys
;;           .
;;           #hasheq((country . "US")
;;                   (id . 4735)
;;                   (message . 0.0043)
;;                   (sunrise . 1543751567)
;;                   (sunset . 1543789684)
;;                   (type . 1)))
;;          (visibility . 16093)
;;          (weather
;;           .
;;           (#hasheq((description . "scattered clouds")
;;                    (icon . "03d")
;;                    (id . 802)
;;                    (main . "Clouds"))))
;;          (wind . #hasheq((deg . 180) (gust . 7.7) (speed . 4.1))))
