#lang info
(define collection "openweather")
(define deps '("opt" "uri" "http11" "tjson" "typed-racket-lib" "base"))
(define build-deps '("scribble-lib" "racket-doc" "typed-racket-lib" "typed-racket-more" "rackunit-lib"))
(define scribblings '(("scribblings/openweather.scrbl" ())))
(define pkg-desc "Call OpenWeatherMap free weather api.")
(define version "0.1.0")
(define pkg-authors '("Raymond Racine"))
